var express = require('express')
var bodyParser = require('body-parser')
var mongoose = require('mongoose');
var routes = require('./routes');


var app = express()
var dbpath = process.env.DB || 'mongodb://localhost:27017/CspReports';
mongoose.connect(dbpath);

app.use(bodyParser.json({
  type: ['json', 'application/csp-report']
}));

routes(app);

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
})